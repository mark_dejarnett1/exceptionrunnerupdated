1. logger.log is different than log to console because in logger.properties the ConsoleHandler level is set at INFO, whereas the FileHandler level is set at ALL.
2. There are two log messages in logger.log that are FINER, they come from the Tester.java functions passingTest() and failOverTest(), because they both throw TimerException.
3. Assertions.assertThrows asserts that execution of the supplied executable throws an exception of the expected type and returns the exception.
4. 
    1. serialVersionUID is a version number associated with a serializable class. We need it because during deserialization to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization.
    2. We need to override the constructors to provide the ability to catch and allow specific treatment to a subset of existing java exceptions. Overriding constructors also allows application users and developers what the problem is when an exception is thrown.
    3. We do not need to override other Exception methods because they are unnecessary, and the overriding of the constructors provides the functionality and usability that we require.
5. The static block in Timer.java opens the logger.properties file in order to establish the properties for each handler for the logger. It is called at the start of execution.
6. README.md file format is Markdown. It is related to Bitbucket because if a repository contains a README.md file at the root level, Bitbucket server displays its contents on the repository's Source page.
7. Test is failing due to the the fact that if a TimerException is thrown, the value timeNow will never be intitialized, so the function will return null creating a NullPointerException which is not the expected exception to be thrown. You must intialize timeNow to 0L, to avoid throwing a NullPointerException.
8. The sequence of exceptions is TimerException, then NullPointerException. The TimerException that is thrown is not handled in the catch block of timeMe(), so then returning a null value throws the NullPointerException, which makes the test fail.
11. TimerException is a Compile Time Exception, and NullPointerException is a Run Time Exception

